module.exports = {
  moduleFileExtensions: ['js', 'json', 'ts'],
  rootDir: '.',
  testRegex: '.spec.ts$',
  transform: {
    '.(ts|tsx)': 'ts-jest',
  },
  resetMocks: true,
  coverageDirectory: './coverage/',
  collectCoverageFrom: [
    '<rootDir>/src/**/*.service.ts',
    '!<rootDir>/src/**/*.external.service.ts',
  ],
  testEnvironment: 'node',
  moduleNameMapper: {
    '^src/(.*)$': '<rootDir>/',
  },
  globals: {
    'ts-jest': {
      compiler: 'ttypescript',
    },
  },
  setupFiles: ['<rootDir>config.ts'],
};
