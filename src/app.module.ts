import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { HealthModule } from './modules/health/health.module';
import { FilmModule } from './modules/film/film.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    MongooseModule.forRoot(
      `${process.env.MONGO_DB_URL}`.replace('{{database}}', 'get-movies'),
      { useUnifiedTopology: true },
    ),
    HealthModule,
    FilmModule,
  ],
})
export class AppModule {}
