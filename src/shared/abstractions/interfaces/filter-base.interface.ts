export interface IBaseFilter {
  limit: number;
  offset: number;
}
