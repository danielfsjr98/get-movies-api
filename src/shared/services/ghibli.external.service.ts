import { HttpService } from '@nestjs/axios';
import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { AxiosRequestConfig } from 'axios';
import { IFilm } from '../abstractions/interfaces/film.interface';

@Injectable()
export class GhibliServices {
  private readonly endpoint: string;

  constructor(private readonly httpService: HttpService) {
    this.endpoint = process.env.GHIBLI_ENDPOINT;
  }

  public async getFilmsByLimit(limit = 10): Promise<IFilm[]> {
    const options: AxiosRequestConfig = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const endpoint = `${this.endpoint}/films?limit=${limit}`;

    return this.httpService
      .get(endpoint, options)
      .toPromise()
      .then((res) => {
        return res.data;
      })
      .catch((err) => {
        if (err.response && err.response.data && err.response.data.statusCode) {
          throw new HttpException(
            err.response.data.messagesDetail,
            err.response.data.statusCode,
          );
        } else {
          throw new HttpException(err, HttpStatus.INTERNAL_SERVER_ERROR);
        }
      });
  }
}
