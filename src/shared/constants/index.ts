export const SWAGGER_PATH = 'api/get-movies/docs';
export const SWAGGER_TITLE = 'Get Movies API';
export const SWAGGER_DESCRIPTION = 'Get Movies documentation';
export const SWAGGER_VERSION = '1.0.1';
