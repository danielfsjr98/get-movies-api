import { IBaseFilter } from '../../abstractions/interfaces/filter-base.interface';
import { IsNumber, IsOptional, IsInt } from 'class-validator';
import { toNumber } from '../../helpers/cast.helper';
import { Transform } from 'class-transformer';

export class BaseFilterReqDto implements IBaseFilter {
  @Transform(({ value }) => toNumber(value, { default: 1, min: 1 }))
  @IsInt()
  @IsNumber(
    {},
    {
      message: (o) => `the ${o.property} value must be a number`,
    },
  )
  @IsOptional()
  limit = 10;

  @Transform(({ value }) => toNumber(value, { default: 0, min: 0 }))
  @IsInt()
  @IsNumber(
    {},
    {
      message: (o) => `the ${o.property} value must be a number`,
    },
  )
  @IsOptional()
  offset = 0;
}
