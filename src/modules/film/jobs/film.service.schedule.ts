import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { FilmService } from '../services/film.service';

@Injectable()
export class FilmServiceSchedule extends FilmService {
  @Cron('0 17 * * *') // run job every day at 17:00am
  async getDomainsSchedule() {
    await this.getFilmsJob();
  }
}
