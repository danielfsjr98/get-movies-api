export interface IFilmSummary {
  title: string;
  originalTitle: string;
  description: string;
  releaseDate: number;
  score: string;
}
