import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { IFilmSummary } from '../abstractions/interfaces/film-summary.interface';

@Schema({ collection: 'film' })
export class FilmEntity implements IFilmSummary {
  @Prop({ required: true })
  title: string;

  @Prop({ required: true })
  originalTitle: string;

  @Prop({ required: true })
  description: string;

  @Prop({ required: true })
  releaseDate: number;

  @Prop({ required: true })
  score: string;
}

export type FilmDocument = FilmEntity & Document;

export const FilmSchema = SchemaFactory.createForClass(FilmEntity);

FilmSchema.index({ releaseDate: 1 });
