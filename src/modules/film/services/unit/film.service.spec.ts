import { FilmService } from '../film.service';
import { mock } from 'jest-mock-extended';
import { Model } from 'mongoose';
import { FilmEntity } from '../../entities/film.entity';
import { GhibliServices } from '../../../../shared/services/ghibli.external.service';
import { baseFilterMock, filmListMock } from './mocks';
import { Logger } from '@nestjs/common';

let sut: FilmService;
let filmRepository: Model<FilmEntity>;
let ghibliServices: GhibliServices;
let logger: Logger;

describe('FilmService', () => {
  beforeAll(() => {
    filmRepository = mock<Model<FilmEntity>>({
      findOneAndUpdate: jest.fn(),
    } as any);
    ghibliServices = mock<GhibliServices>();
    logger = mock<Logger>();

    sut = new FilmService(filmRepository, ghibliServices, logger);
  });

  afterEach(() => {
    jest.clearAllMocks();
    jest.restoreAllMocks();
  });

  it('should be defined', () => {
    expect(filmRepository).toBeDefined();
  });

  describe('Function: buildAggregate', () => {
    it('should return correct aggregate', () => {
      let error;
      let result;

      try {
        result = sut['buildAggregate'](baseFilterMock);
      } catch (err) {
        error = err;
      }

      expect(result).toEqual([
        { $sort: { releaseDate: 1 } as any },
        {
          $facet: {
            metadata: [
              { $count: 'total' },
              { $addFields: { offset: baseFilterMock.offset } },
            ],
            items: [
              { $skip: baseFilterMock.offset },
              { $limit: baseFilterMock.limit },
            ],
          },
        },
      ]);
      expect(error).not.toBeDefined();
    });
  });

  describe('Function: list', () => {
    let buildAggregateSpyOn: jest.SpyInstance;

    beforeEach(() => {
      buildAggregateSpyOn = jest
        .spyOn(sut as any, 'buildAggregate')
        .mockImplementation(() => ['any_stage']);

      filmRepository.aggregate = jest.fn().mockImplementation(() => ({
        exec: () => ['any_result'],
      }));
    });

    it('should call "filmRepository" with correct params', async () => {
      let error;

      try {
        await sut.list(baseFilterMock);
      } catch (err) {
        error = err;
      }

      expect(filmRepository.aggregate).toBeCalledWith(
        buildAggregateSpyOn.mock.results[0].value,
      );
      expect(error).not.toBeDefined();
    });

    it('should return the result of this.filmRepository.aggregate().exec()', async () => {
      let error;
      let result;

      try {
        result = await sut.list(baseFilterMock);
      } catch (err) {
        error = err;
      }

      expect(result).toEqual(filmRepository.aggregate().exec());
      expect(error).not.toBeDefined();
    });
  });

  describe('Function: getFilmsJob', () => {
    let updateFilmListSpyOn: jest.SpyInstance;

    beforeEach(() => {
      updateFilmListSpyOn = jest
        .spyOn(sut as any, 'updateFilmList')
        .mockImplementation(() => undefined);

      filmRepository.count = jest.fn().mockResolvedValue(200);
    });

    it('Should not call "updateFilmList" if totalFilms >= 200', async () => {
      let error;

      try {
        await sut.getFilmsJob();
      } catch (err) {
        error = err;
      }

      expect(updateFilmListSpyOn).not.toBeCalled();
      expect(error).not.toBeDefined();
    });

    it('Should call "updateFilmList" if totalFilms < 200', async () => {
      filmRepository.count = jest.fn().mockResolvedValue(199);
      let error;

      try {
        await sut.getFilmsJob();
      } catch (err) {
        error = err;
      }

      expect(updateFilmListSpyOn).toBeCalled();
      expect(error).not.toBeDefined();
    });
  });

  describe('Function: updateFilmList', () => {
    beforeEach(() => {
      ghibliServices.getFilmsByLimit = jest
        .fn()
        .mockResolvedValue(filmListMock);

      filmRepository.findOneAndUpdate = jest.fn().mockImplementation(() => ({
        exec: () => null,
      }));
    });

    it('Should call "filmRepository.findOneAndUpdate" the correct number of times', async () => {
      let error;

      try {
        await sut['updateFilmList']();
      } catch (err) {
        error = err;
      }

      expect(filmRepository.findOneAndUpdate).toBeCalledTimes(
        filmListMock.length,
      );
      expect(error).not.toBeDefined();
    });

    it('Should call "logger.error" if "filmRepository.findOneAndUpdate" throws an error', async () => {
      let error;
      filmRepository.findOneAndUpdate = jest.fn().mockImplementation(() => {
        throw Error();
      });

      try {
        await sut['updateFilmList']();
      } catch (err) {
        error = err;
      }

      expect(logger.error).toBeCalled();
      expect(error).not.toBeDefined();
    });
  });
});
