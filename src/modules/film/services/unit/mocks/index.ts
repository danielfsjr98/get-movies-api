import { IBaseFilter } from '../../../../../shared/abstractions/interfaces/filter-base.interface';
import { createMock, createMockList } from 'ts-auto-mock';
import { IFilm } from '../../../../../shared/abstractions/interfaces/film.interface';

export const baseFilterMock = createMock<IBaseFilter>();

export const filmListMock = createMockList<IFilm>(200);
