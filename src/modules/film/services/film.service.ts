import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { BaseFilterReqDto } from '../../../shared/dtos/req/filter-base.req.dto';
import { GhibliServices } from '../../../shared/services/ghibli.external.service';
import { FilmSummaryResDTO } from '../dtos/res/film-summary.res.dto';
import { FilmEntity } from '../entities/film.entity';

@Injectable()
export class FilmService {
  constructor(
    @InjectModel(FilmEntity.name)
    private readonly filmRepository: Model<FilmEntity>,
    private readonly ghibliServices: GhibliServices,
    private readonly logger: Logger,
  ) {}

  private buildAggregate(filter: BaseFilterReqDto) {
    return [
      { $sort: { releaseDate: 1 } as any },
      {
        $facet: {
          metadata: [
            { $count: 'total' },
            { $addFields: { offset: filter.offset } },
          ],
          items: [{ $skip: filter.offset }, { $limit: filter.limit }],
        },
      },
    ];
  }

  public async list(filter: BaseFilterReqDto): Promise<FilmSummaryResDTO[]> {
    return this.filmRepository
      .aggregate([...this.buildAggregate(filter)])
      .exec();
  }

  public async getFilmsJob() {
    const totalFilms = await this.filmRepository.count();

    if (totalFilms < 200) {
      await this.updateFilmList();
    }
  }

  private async updateFilmList() {
    const logId = 'get-movies.film.filmService.updateFilmList';

    const films = await this.ghibliServices.getFilmsByLimit(200);

    for (const film of films) {
      try {
        await this.filmRepository
          .findOneAndUpdate(
            {
              title: film.title,
              originalTitle: film.original_title,
            },
            {
              $set: {
                title: film.title,
                originalTitle: film.original_title,
                description: film.description,
                releaseDate: parseInt(film.release_date),
                score: film.rt_score,
              },
            },
            { upsert: true, new: true },
          )
          .exec();
      } catch (err) {
        this.logger.error(JSON.stringify(err), '', logId);
      }
    }
  }
}
