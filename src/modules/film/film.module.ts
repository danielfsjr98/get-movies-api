import { Logger, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { FilmEntity, FilmSchema } from './entities/film.entity';
import { FilmController } from './film.controller';
import { FilmService } from './services/film.service';
import { FilmServiceSchedule } from './jobs/film.service.schedule';
import { ScheduleModule } from '@nestjs/schedule';
import { GhibliServices } from '../../shared/services/ghibli.external.service';
import { HttpModule } from '@nestjs/axios';

@Module({
  imports: [
    ScheduleModule.forRoot(),
    MongooseModule.forFeature([
      {
        name: FilmEntity.name,
        schema: FilmSchema,
        collection: 'film',
      },
    ]),
    HttpModule,
  ],
  controllers: [FilmController],
  providers: [FilmService, FilmServiceSchedule, GhibliServices, Logger],
})
export class FilmModule {}
