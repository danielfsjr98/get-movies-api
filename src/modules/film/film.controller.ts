import {
  Controller,
  Get,
  Query,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { ApiQuery, ApiTags } from '@nestjs/swagger';
import { BaseFilterReqDto } from '../../shared/dtos/req/filter-base.req.dto';
import { FilmSummaryResDTO } from './dtos/res/film-summary.res.dto';
import { FilmService } from './services/film.service';

@ApiTags('film')
@Controller('api/get-movies/film')
export class FilmController {
  constructor(private readonly filmService: FilmService) {}

  @Get('')
  @ApiQuery({ name: 'limit', type: 'string', required: false })
  @ApiQuery({ name: 'offset', type: 'string', required: false })
  @UsePipes(new ValidationPipe())
  async filterFilms(
    @Query(
      new ValidationPipe({
        transform: true,
        transformOptions: { enableImplicitConversion: true },
        forbidNonWhitelisted: true,
      }),
    )
    filter: BaseFilterReqDto,
  ): Promise<FilmSummaryResDTO[]> {
    return this.filmService.list(filter);
  }
}
