import { IFilmSummary } from '../../abstractions/interfaces/film-summary.interface';

export class FilmSummaryResDTO implements IFilmSummary {
  title: string;
  originalTitle: string;
  description: string;
  releaseDate: number;
  score: string;
}
